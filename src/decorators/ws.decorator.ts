import {Injectable} from 'ts-fw';

export function WS(): ClassDecorator {
    return (constructor: Function) => {
        Reflect.decorate([Injectable('WebSocket')], constructor);
    }
}
