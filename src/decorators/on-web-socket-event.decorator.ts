export interface IOnWebSocketEventModel {
    event: string;
    method: string;
}

export function OnWebSocketEvent(event: string): MethodDecorator {
    return (constructor: any, propertyKey: string) => {
        let methods: IOnWebSocketEventModel[];

        if (Reflect.hasMetadata('methods', constructor)) {
            methods = Reflect.getMetadata('methods', constructor);
        } else {
            methods = [];
            Reflect.defineMetadata('methods', methods, constructor);
        }

        methods.push({
            event: event,
            method: propertyKey
        });
    }
}
