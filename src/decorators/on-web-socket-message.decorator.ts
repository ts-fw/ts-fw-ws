export interface IOnWebSocketMessageModel {
    isMessageHandler: boolean;
    method: string;
}

export function OnWebSocketMessage(): MethodDecorator {
    return (constructor: any, propertyKey: string) => {
        let methods: IOnWebSocketMessageModel[];

        if (Reflect.hasMetadata('methods', constructor)) {
            methods = Reflect.getMetadata('methods', constructor);
        } else {
            methods = [];
            Reflect.defineMetadata('methods', methods, constructor);
        }

        methods.push({
            method: propertyKey,
            isMessageHandler: true
        });
    }
}
