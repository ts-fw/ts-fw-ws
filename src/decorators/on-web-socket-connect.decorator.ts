export function OnWebSocketConnect(): MethodDecorator {
    return (constructor: any, propertyKey: string) => {
        Reflect.defineMetadata('OnWebSocketConnect', propertyKey, constructor);
    }
}
