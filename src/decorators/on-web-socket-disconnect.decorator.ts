export function OnWebSocketDisconnect(): MethodDecorator {
    return (constructor: any, propertyKey: string) => {
        Reflect.defineMetadata('OnWebSocketDisconnect', propertyKey, constructor);
    }
}
