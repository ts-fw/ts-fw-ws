import {inject, Container} from 'inversify';
import {Config, EConfig, IConfigStorage} from 'ts-fw';
import {WebSocketsService} from '../services';
import {IWebSocketServerOptionsConfigModel, WebSocketEventDataValidationModel, WebSocket} from '../models';
import * as winston from 'winston';
import {IOnWebSocketEventModel, IOnWebSocketMessageModel} from '../decorators';
import {Server} from 'http';
import {Data, Server as WebSocketServer} from 'ws';
import {Validator} from 'class-validator';
import * as shortId from 'shortid';

@Config(2, EConfig.Server)
export class WebSocketConfig {
    @inject('logger')
    private logger: winston.Winston;

    @inject('configStorage')
    private configStorage: IConfigStorage;

    @inject('webSocketsService')
    private webSocketsService: WebSocketsService;

    @inject('di')
    private container: Container;

    @inject('validatorService')
    private validatorService: Validator;

    async configuring(server: Server): Promise<void> {
        const webSocketConfig = this.configStorage.get<IWebSocketServerOptionsConfigModel>('ws');
        webSocketConfig.server = <Server>server;

        const webSocketServer = new WebSocketServer(webSocketConfig);
        this.container.bind('wsServer').toConstantValue(webSocketServer);

        webSocketServer.on('connection', async (socket: WebSocket) => {
            socket.id = shortId.generate();
            this.logger.debug('ts-fw-ws', `Socket connected; id: ${socket.id}`);
            this.webSocketsService.getSocketsList().push(socket);

            const webSocketControllers: any[] = [];

            try {
                webSocketControllers.push(...this.container.getAll('WebSocket'));
            } catch (ignore) {
            }

            for (const service of webSocketControllers) {
                if (Reflect.hasMetadata('OnWebSocketConnect', service)) {
                    const methodName = Reflect.getMetadata('OnWebSocketConnect', service);
                    await service[methodName](socket);
                }
            }

            socket.on('close', async () => {
                this.logger.debug('ts-fw-ws', `Socket disconnect; id: ${socket.id}`);
                const index = this.webSocketsService.getSocketsList().findIndex(webSocket => webSocket === socket);
                this.webSocketsService.getSocketsList().splice(index, 1);

                for (const service of webSocketControllers) {
                    if (!Reflect.hasMetadata('OnWebSocketDisconnect', service)) {
                        continue;
                    }

                    await service[Reflect.getMetadata('OnWebSocketDisconnect', service)](socket);
                }
            });

            socket.on('message', async (data: Data) => {
                this.logger.debug('ts-fw-ws', `Incoming message; data: ${data.toString()}`);

                const str = data.toString();

                const controllers = webSocketControllers.filter(controller => {
                    return Reflect.hasMetadata('methods', controller);
                });

                const webSocketEventData = new WebSocketEventDataValidationModel();

                try {
                    const json = JSON.parse(str);
                    webSocketEventData.event = json.event;
                    webSocketEventData.data = json.data;
                    await this.validatorService.validateOrReject(webSocketEventData);
                } catch (ignore) {
                    controllers.forEach(controller => {
                        const onWebSocketEventModels: IOnWebSocketMessageModel[] = Reflect.getMetadata('methods', controller) || [];
                        const eventModels = onWebSocketEventModels.filter(model => {
                            return model.isMessageHandler === true;
                        });

                        eventModels.forEach(eventModel => {
                            controller[eventModel.method](socket, str);
                        });
                    });

                    return;
                }

                controllers.forEach(controller => {
                    const onWebSocketEventModels: IOnWebSocketEventModel[] = Reflect.getMetadata('methods', controller) || [];
                    const eventModels = onWebSocketEventModels.filter(model => {
                        return model.event === webSocketEventData.event;
                    });

                    eventModels.forEach(eventModel => {
                        controller[eventModel.method](socket, webSocketEventData.data);
                    });
                });
            });
        });
    }
}
