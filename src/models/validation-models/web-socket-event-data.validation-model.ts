import {IsString, IsNotEmpty, IsAscii, IsOptional} from 'class-validator';

export class WebSocketEventDataValidationModel {
    @IsString()
    @IsNotEmpty()
    @IsAscii()
    public event: string;

    @IsOptional()
    public data?: any;
}
