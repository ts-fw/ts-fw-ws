import {Service} from 'ts-fw';
import {WebSocket} from '../models';

@Service()
export class WebSocketsService {
    private readonly sockets: WebSocket[] = [];

    getSocketsList(): WebSocket[] {
        return this.sockets;
    }
}
